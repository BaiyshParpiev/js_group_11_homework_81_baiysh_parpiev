const mongoose = require('mongoose');

const linksSchema = new mongoose.Schema({
    shortUrl: {
        type: String,
        required: true
    },
    originalUrl: {
        type: String,
        required: true
    }
});

const Links = mongoose.model('links', linksSchema);

module.exports = Links;