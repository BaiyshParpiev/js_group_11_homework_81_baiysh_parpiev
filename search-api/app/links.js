const express = require('express');
const {nanoid} = require('nanoid');

const router = express.Router();
const Links = require('../models/Links');
const {models} = require("mongoose");

router.get('/:id', async(req, res) => {
    try{
        const link = await Links.findOne({shortUrl: req.params.id});
        if(link){
            res.status(301).redirect(link.originalUrl)
        }else{
            return res.status(404).send({error: 'Link not Found'});
        }
    }catch {
        res.sendStatus(500);
    }
});


router.post('/', async(req, res) => {
    if(!req.body.link){
        return res.status(404).send({error: 'Date not valid'});
    }
    let short = nanoid(6)
    const find = await Links.findOne({shortUrl: short});
    let linkData;

    if(find){
        short = nanoid(6)
    }else{
        linkData = {
            originalUrl: req.body.link,
            shortUrl: short,
        }
        console.log(linkData)
    }
    const link = new Links(linkData);
    try{
        await link.save();
        res.send(link)
    }catch{
        res.sendStatus(400).send('Data not valid')
    }
});
module.exports = router;