import React from 'react';
import {Button, Grid, makeStyles, TextField} from "@material-ui/core";
import Result from "../Result/Result";
import {useDispatch, useSelector} from "react-redux";
import {createLink, onChange} from "../../store/actions/actions";
const useStyles = makeStyles(({
    container: {
        position: 'absolute',
        top: '50%',
    },
    input: {
        marginBottom: '40px',
    }
}))

const Link = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {link} = useSelector(state => state);

    const createHandler = () => {
        dispatch(createLink({link: link}));
    }


    return (
        <Grid container direction='column' className={classes.container}>
            <Grid item container justifyContent="center" xs={11} className={classes.input}>
                <TextField
                    label="Enter here to short your link"
                    onChange={e => dispatch(onChange(e.target))}
                    name="link"
                    required
                    fullWidth
                    variant="outlined"
                    value={link}
                />
            </Grid>
            <Grid item container justifyContent="center">
                <Button type="submit" color="primary" variant="contained" onClick={createHandler}>Short</Button>
            </Grid>
            <Result/>
        </Grid>
    );
};

export default Link;