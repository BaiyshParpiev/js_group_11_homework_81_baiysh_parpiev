import React from 'react';
import {Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";

const Result = () => {
    const {shortLink} = useSelector(state => state);


    return shortLink && (
        <Grid item container direction='column' justifyContent="center" xs>
            <Typography variant="body1">Your link looks like this</Typography>
            <a href={`http://localhost:8000/links/${shortLink}`}>https://localhost:8000/{shortLink}</a>
        </Grid>
    );
};

export default Result;