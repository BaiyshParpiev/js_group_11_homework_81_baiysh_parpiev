import axios from "axios";

const url = "http://localhost:8000/links";

export const CREATE_LINK_REQUEST = 'CREATE_LINK_REQUEST';
export const CREATE_LINK_SUCCESS = 'CREATE_LINK_SUCCESS';
export const CREATE_LINK_FAILURE = 'CREATE_LINK_FAILURE';

export const ON_CHANGE = 'ON_CHANGE';
export const onChange = ({name, value}) => ({type: ON_CHANGE, payload: {name, value}})


export const createLinkRequest = () => ({type: CREATE_LINK_REQUEST})
export const createLinkSuccess = link => ({type: CREATE_LINK_SUCCESS, payload: link});
export const createLinkFailure = e => ({type: CREATE_LINK_FAILURE, payload: e});

export const createLink = info => {
    return async dispatch => {
        try {
            dispatch(createLinkRequest());
            const {data} = await axios.post(url, info);
            console.log(data)
            dispatch(createLinkSuccess(data.shortUrl));
        } catch (e) {
            dispatch(createLinkFailure(e));
        }
    }
};




