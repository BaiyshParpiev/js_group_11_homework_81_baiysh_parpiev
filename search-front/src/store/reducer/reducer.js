import {
    CREATE_LINK_FAILURE,
    CREATE_LINK_REQUEST,
    CREATE_LINK_SUCCESS, ON_CHANGE
} from "../actions/actions";

const initialState = {
    shortLink: null,
    link: '',
    fetchLoading: false,
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type){
        case ON_CHANGE:
            return {
                ...state,
                [action.payload.name]: action.payload.value
            }
        case CREATE_LINK_REQUEST:
            return {...state, fetchLoading: true};
        case CREATE_LINK_SUCCESS:
            return {...state, fetchLoading: false, shortLink: action.payload};
        case CREATE_LINK_FAILURE:
            return {...state, fetchLoading: false};
        default:
            return state;
    }
};

export default reducer;
